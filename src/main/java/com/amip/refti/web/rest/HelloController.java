package com.amip.refti.web.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/hello")
public class HelloController {

  @Value("${refti.k8s.key}")
  private String k8sKey;
  @Value("${refti.trd.ak}")
  private String trdAk;
  @Value("${refti.trd.sk}")
  private String trdSk;


  @GetMapping("/env")
  public ResponseEntity env() {
    Map<String, Object> dat = new HashMap<>();
    dat.put("refti.k8s.key", this.k8sKey);
    dat.put("refti.trd.ak", this.trdAk);
    dat.put("refti.trd.sk", this.trdSk);
    Map<String, String> envmap = System.getenv();
    dat.put("env", envmap);
    return ResponseEntity.ok(dat);
  }


}
