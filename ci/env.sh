#!/bin/sh
#
#
#

#set -v

BIN_PATH=$(dirname $(readlink -f $0))

#ENV_FILE_DEF=$(dirname $(readlink -f $0))/env/env.develop.sh
ENV_FILE=${BIN_PATH}/env/env.${BRANCH_NAME}.sh

if [ ! -f "$ENV_FILE" ]; then
#  echo "环境变量文件 ${ENV_FILE} 不存在, 使用默认 ${DEF_ENV_FILE} 环境变量文件"
#  ENV_FILE=${DEF_ENV_FILE}

  echo "环境变量文件 ${ENV_FILE} 不存在, 停止后续构建"
  exit 1
fi


echo "环境变量文件 ->  ${ENV_FILE}"

. ${ENV_FILE}
#source ${ENV_FILE}
#sh -f ${ENV_FILE}

