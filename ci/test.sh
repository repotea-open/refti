#!/bin/sh
#
#
#

NAME=
IMAGE_TAG=latest
BRANCH=
TAG=
DOCKERFILE=
DIRECTORY=


_help() {
  echo ''
  echo 'Usage: image.sh -n (--name)          * Image name'
  echo '                -e (--image-version) * Image version'
  echo '                -b (--branch)        x Git branch (branch or tag need one)'
  echo '                -t (--tag)           x Git tag    (branch or tag need one)'
  echo '                -f (--file)          * Dockerfile path'
  echo '                -d (--directory)     * Dockerfile base path'
  echo '                -h (--help)          Print this document'
  exit 1 # Exit script after printing help
}

while [ -n "$1" ]
do
  case "$1" in
    -n|--name)
      NAME=$2
      ;;
    -e|--image-version)
      IMAGE_TAG=$2
      ;;
    -b|--branch)
      BRANCH=$2
      ;;
    -t|--tag)
      TAG=$2
      ;;
    -f|--file)
      DOCKERFILE=$2
      ;;
    -d|--directory)
      DIRECTORY=$2
      ;;
    *)
      ;;
  esac
  shift
done


if [ -z $NAME ]; then
  echo 'build image script lost -n (--name) flag'
  exit 1;
fi
if [ -z ${IMAGE_TAG} ]; then
  echo 'build image script lost -e (--image-version) flag'
  exit 1;
fi
if [ -z ${DOCKERFILE} ]; then
  echo 'build image script lost -f (--file) flag'
  exit 1;
fi
if [ -z ${DIRECTORY} ]; then
  echo 'build image script lost -d (--directory) flag'
  exit 1;
fi
if [ -z $BRANCH ] && [ -z $TAG ]; then
  echo 'build image script lost -t (--tag) or -b (--branch) flag'
  exit 1;
fi


echo name: ${NAME}
echo branch: ${BRANCH}
echo tag: ${TAG}
echo dockerfile: ${DOCKERFILE}
echo directory: ${DIRECTORY}

echo ${NAME}:$([ -z "${BRANCH}" ] && echo ${TAG} || echo ${BRANCH})

# sh -f ci/test.sh \
#   --name      refti \
#   --branch    master \
#   --tag       x \
#   --file      /path/to/dockerfile \
#   --directory /path/to/basepath
