#!/bin/bash
#
#
#


set -v


BIN_PATH=$(dirname $(readlink -f $0))
export WORK_PATH=$BIN_PATH/../

docker-compose -f ${WORK_PATH}/ci/build/package.yml \
  -p ${PROJECT_NAME} \
  up \
  --abort-on-container-exit


sed -i 's/${WORK_PATH}/'$(echo $WORK_PATH | sed 's_/_\\/_g')'/g' ${BIN_PATH}/docker/source.dockerfile || exit 1

cat ${BIN_PATH}/docker/source.dockerfile

sh -f ${BIN_PATH}/build/image.sh \
  --name          ${IMAGE_REPOSITORY}-code \
  --image-tag     ${IMAGE_TAG} \
  ${BRANCH_NAME:+--git-branch  ${BRANCH_NAME}} \
  ${CI_COMMIT_TAG:+--git-tag   ${CI_COMMIT_TAG}} \
  --file          ${BIN_PATH}/docker/source.dockerfile \
  --directory     ${WORK_PATH} || exit 1
  # --directory     / || exit 1



sh -f ${BIN_PATH}/build/image.sh \
  --name          ${IMAGE_REPOSITORY} \
  --image-tag     ${IMAGE_TAG} \
  ${BRANCH_NAME:+--git-branch  ${BRANCH_NAME}} \
  ${CI_COMMIT_TAG:+--git-tag   ${CI_COMMIT_TAG}} \
  --file          ${BIN_PATH}/docker/app.dockerfile \
  --directory     ${WORK_PATH}/target || exit 1


