#!/bin/sh
#
#
#

#set -v

BIN_PATH=$(dirname $(readlink -f $0))
WORK_PATH=$BIN_PATH/../

. ${WORK_PATH}/ci/env/env.$([ -n "${BRANCH_NAME}" ] && echo ${BRANCH_NAME} || echo 'default').sh || exit 1

DEPLOY_FILE=${BIN_PATH}/apply/deploy.yml
SERVICE_FILE=${BIN_PATH}/apply/service.yml
INGRESS_FILE=${BIN_PATH}/apply/ingress.yml
M2PVC_FILE=${BIN_PATH}/apply/m2pvc.yml

echo "image url: ${IMAGE_REPO}/refti:${IMAGE_TAG}"

sed -i 's/${IMAGE_TAG}/'${IMAGE_TAG}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${REPLICAS}/'${REPLICAS}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${IMAGE_REPO}/'${IMAGE_REPO}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${IMAGE_SECRET}/'${IMAGE_SECRET}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${SPRING_PROFILES_ACTIVE}/'${SPRING_PROFILES_ACTIVE}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${MYSQL_HOST}/'${MYSQL_HOST}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${MYSQL_PORT}/'${MYSQL_PORT}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${MYSQL_DATABASE}/'${MYSQL_DATABASE}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${MYSQL_USER}/'${MYSQL_USER}'/g' ${DEPLOY_FILE} || exit 1
sed -i 's/${MYSQL_PASSWORD}/'${MYSQL_PASSWORD}'/g' ${DEPLOY_FILE} || exit 1

sed -i 's/${DOMAIN}/'${K8S_INGRESS_DOMAIN}'/g' ${INGRESS_FILE} || exit 1

echo $kube_config |base64 -d > $HOME/.kube/config

kubectl apply -f ${M2PVC_FILE} -n ${K8S_NAMESPACE} --kubeconfig=${HOME}/.kube/config.aks || exit 1
kubectl apply -f ${BIN_PATH}/certmanager -n ${K8S_NAMESPACE} --kubeconfig=${HOME}/.kube/config.aks || exit 1
kubectl apply -f ${INGRESS_FILE} -n ${K8S_NAMESPACE} --kubeconfig=${HOME}/.kube/config.aks || exit 1
kubectl apply -f ${SERVICE_FILE} -n ${K8S_NAMESPACE} --kubeconfig=${HOME}/.kube/config.aks || exit 1
kubectl apply -f ${DEPLOY_FILE} -n ${K8S_NAMESPACE}  --kubeconfig=${HOME}/.kube/config.aks || exit 1
