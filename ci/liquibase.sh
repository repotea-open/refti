#!/bin/sh
#
#
#

set -v

BIN_PATH=$(dirname $(readlink -f $0))
export WORK_PATH=$BIN_PATH/../


docker-compose -f ${WORK_PATH}/ci/build/liquibase.yml \
  -p ${PROJECT_NAME} \
  up \
  --abort-on-container-exit

