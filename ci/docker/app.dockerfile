FROM openjdk:8

MAINTAINER chilunyc "ceshi@chilunyc.com"

RUN mkdir -p /app
COPY *.jar /app/app.jar
WORKDIR /app

CMD java -jar app.jar
