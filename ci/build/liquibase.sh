#!/bin/sh
#
#
#



set -v

BIN_PATH=$(dirname $(readlink -f $0))
WORK_PATH=$BIN_PATH/../../

. ${WORK_PATH}/ci/env/env.$([ -n "${BRANCH_NAME}" ] && echo ${BRANCH_NAME} || echo 'default').sh || exit 1

#echo 'liquibase host -> ' ${MYSQL_HOST}

# ${WORK_PATH}/mvnw
mvn clean compile liquibase:${@} \
  ${MYSQL_HOST:+-D liquibase.host=${MYSQL_HOST}} \
  ${MYSQL_PORT:+-D liquibase.port=${MYSQL_PORT}} \
  ${MYSQL_DATABASE:+-D liquibase.schema=${MYSQL_DATABASE}} \
  ${MYSQL_USER:+-D liquibase.username=${MYSQL_USER}} \
  ${MYSQL_PASSWORD:+-D liquibase.password=${MYSQL_PASSWORD}}

