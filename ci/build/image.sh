#!/bin/bash
# 基于 AWS ECR 创建 docker 镜像
# 前提条件:
#   1、在创建镜像的服务器上安装 AWS CLI
#   2、配置身份认证信息
#       access-key-id
#       access-key-secret
#       region


# 镜像名称
NAME=
# 镜像版本
IMAGE_TAG=latest
# git 分支
GIT_BRANCH=
# git 标签
GIT_TAG=
# 构建镜像 Dockerfile 文件
DOCKERFILE=
# 构建镜像路径
DIRECTORY=

while [ -n "$1" ]
do
  case "$1" in
    -n|--name)
      NAME=$2
      ;;
    -e|--image-tag)
      IMAGE_TAG=$2
      ;;
    -b|--git-branch)
      GIT_BRANCH=$2
      ;;
    -t|--git-tag)
      GIT_TAG=$2
      ;;
    -f|--file)
      DOCKERFILE=$2
      ;;
    -d|--directory)
      DIRECTORY=$2
      ;;
    *)
      ;;
  esac
  shift
done


if [ -z ${NAME} ]; then
  echo 'build image script lost -n (--name) flag'
  exit 1;
fi
if [ -z ${IMAGE_TAG} ]; then
  echo 'build image script lost -e (--image-tag) flag'
  exit 1;
fi
if [ -z ${DOCKERFILE} ]; then
  echo 'build image script lost -f (--file) flag'
  exit 1;
fi
if [ -z ${DIRECTORY} ]; then
  echo 'build image script lost -d (--directory) flag'
  exit 1;
fi
if [ -z ${GIT_BRANCH} ] && [ -z ${GIT_TAG} ]; then
  echo 'build image script lost -t (--tag) or -b (--branch) flag'
  exit 1;
fi



echo '开始创建镜像......................................................'

  echo "  第一步: 为项目: ${NAME} 创建对应的仓库"
  REPOSITORY=$(aws ecr list-images --repository-name ${NAME})
  if [ -n "$REPOSITORY" ]; then
    echo "      项目 ${NAME} 的仓库已存在"
  else
    echo "创建仓库 ${NAME}"
    aws ecr create-repository --repository-name ${NAME}
    echo "为仓库 ${NAME} 增加生命周期策略"
    aws ecr put-lifecycle-policy --repository-name ${NAME} --lifecycle-policy-text '{"rules":[{"rulePriority":1,"description":"删除没有标签的images","selection":{"tagStatus":"untagged","countType":"sinceImagePushed","countUnit":"days","countNumber":1},"action":{"type":"expire"}}]}'
  fi

  echo "  第二步: 获取仓库 repositoryUri"
  DESCRIBE=$(aws ecr describe-repositories --repository-name ${NAME})
  DESCRIBE=${DESCRIBE##*repositoryUri\": \"}
  REPOSITORY_URI=${DESCRIBE%%\"*}
  echo "    => REPOSITORY_URI: ${REPOSITORY_URI}"

  echo "  第三步: 获取 docker login 命令并执行"
  DOCKER_LOGIN=$(aws ecr get-login --no-include-email)
  if [ -n "$DOCKER_LOGIN" ]; then
    $DOCKER_LOGIN || exit 1
  fi

  echo "  第四步: 创建镜像"

  docker build -t ${NAME}:${IMAGE_TAG} -f ${DOCKERFILE} ${DIRECTORY} || exit 1
  docker tag ${NAME}:${IMAGE_TAG} ${REPOSITORY_URI}:${IMAGE_TAG} || exit 1
  docker push ${REPOSITORY_URI}:${IMAGE_TAG} || exit 1
  echo "      => ${REPOSITORY_URI}:${IMAGE_TAG}"

  echo "  第五步: 上传到 ECR"

  if [ -n "${GIT_BRANCH}" ]; then
    echo "    -> 上传 ${GIT_BRANCH} 分支镜像"
    docker tag ${NAME}:${IMAGE_TAG} ${REPOSITORY_URI}:${GIT_BRANCH} || exit 1
    docker push ${REPOSITORY_URI}:${GIT_BRANCH} || exit 1
    echo "      => ${REPOSITORY_URI}:${GIT_BRANCH}"
  fi

  if [ -n "${GIT_TAG}" ]; then
    echo "    -> 上传 ${GIT_TAG} 标签以及 latest 镜像"
    docker tag ${NAME}:${IMAGE_TAG} ${REPOSITORY_URI}:${GIT_TAG} || exit 1
    docker tag ${NAME}:${IMAGE_TAG} ${REPOSITORY_URI}:latest || exit 1
    docker push ${REPOSITORY_URI}:${GIT_TAG} || exit 1
    docker push ${REPOSITORY_URI}:latest || exit 1
    echo "      => ${REPOSITORY_URI}:${GIT_TAG}"
    echo "      => ${REPOSITORY_URI}:latest"
  fi

  echo "  第六步: 清除本机已上传的镜像"

  if [ -n "${GIT_BRANCH}" ]; then
    echo "    -> 清除本机 ${GIT_BRANCH} 分支镜像"
    docker rmi ${REPOSITORY_URI}:${GIT_BRANCH} || exit 1
  fi

  if [ -n "${GIT_TAG}" ]; then
    echo "    -> 清除本机 ${GIT_TAG} 以及 latest 镜像"
    docker rmi ${REPOSITORY_URI}:${GIT_TAG} || exit 1
    docker rmi ${REPOSITORY_URI}:latest || exit 1
  fi

  docker rmi ${REPOSITORY_URI}:${IMAGE_TAG} || exit 1
  docker rmi ${NAME}:${IMAGE_TAG} || exit 1

#  echo "  第七步: 清除所有无标签镜像"
#  docker rmi $(docker images | grep "none" | awk '{print $3}')

echo '创建镜像结束......................................................'
